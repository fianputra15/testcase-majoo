import React from "react";
import {Card, Col} from "react-bootstrap";
import CardHeader from "react-bootstrap/CardHeader";
import {Cards} from "../../molecules";

export const GroupCard = (props) => {
    return (
        <Col lg={4} md={6} xs={12}>
            <Card style={{cursor: 'pointer', padding: '12px', width: '20.5rem'}}>
                <CardHeader>{props.header}</CardHeader>
                <br/>
                {props.stateList?.map(value => {
                    if(!props.type){
                        if(!value?.status) {
                            return (
                                <>
                                    <Cards
                                        {...value}
                                        clickCard={props.openCard}
                                    />
                                </>
                            );
                        }
                    }else{
                        if(value?.status) {

                            return (
                                <>
                                    <Cards
                                        {...value}
                                        clickCard={props.openCard}
                                    />
                                </>
                            );
                        }
                    }
                })
                }
            </Card>
        </Col>
    )
}
