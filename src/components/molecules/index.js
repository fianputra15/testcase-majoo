import {Cards} from "./Card";
import {Header} from "./Header";
import {DetailCard} from "./DetailCard";
export {
    Cards,
    DetailCard,
    Header
};