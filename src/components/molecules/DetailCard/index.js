import React from "react";
import {Badge, Button} from "react-bootstrap";
import moment from "moment";

export const DetailCard = (props) => {
    return (
        <>
            <p>{props?.description}</p>
            <Badge bg={`success`} className="mb-5">{moment(props?.createdAt).format("DD/MM/YYYY hh:mm")}</Badge>
            <br/>
            <div style={{textAlign: 'right'}}>
                <Button style={{margin: '6px'}} variant="info" onClick={() => props.openFormUpdate()}>Edit</Button>
                {(!props?.status) && (<Button style={{margin: '6px'}} variant="danger" onClick={() => props.deleteTODO(props?.id)}>Delete</Button>)}
            </div>
        </>
    )
}
