import React from "react";
import {Badge} from "react-bootstrap";
import {Card} from "react-bootstrap";
export const Cards = (props) => {
    return (
        <>
            <Card style={{width: '18rem', margin: '8px'}} onClick={() => props.clickCard(props)}>
                <Card.Body>
                    <Card.Title>{props?.title}</Card.Title>
                    <Card.Text>
                        <p>{props?.description}</p>
                        <Badge
                            bg={`success`}>{props?.createdAt}</Badge>
                    </Card.Text>
                </Card.Body>
            </Card>
        </>
    )
}

