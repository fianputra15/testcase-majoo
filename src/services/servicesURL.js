require('dotenv').config();
const serverUrl = process.env.REACT_APP_BACKEND_URL;
export default serverUrl;
