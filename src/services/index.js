import servicesURL from "./servicesURL";
import axios from "axios";

const baseUrl = axios.create({
    baseURL: servicesURL
})

export const getListTodo = (id) => {
    return baseUrl.get(id).then((res) => res.data);
}
