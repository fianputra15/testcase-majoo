import React, {useEffect, useState} from "react";
import {Form, Button, Container, Modal, ModalBody, Row} from "react-bootstrap";
import {getListTodo} from "../services";
import moment from "moment";
import {DetailCard, GroupCard, Header} from "../components";
import {isEmpty} from "lodash";

const Home = () => {
    const [activeCard, setActiveCard] = useState([]);
    const [stateList, setStateList] = useState([]);
    const [stateOpenFormCreate, setStateOpenFormCreate] = useState(false);
    const [stateOpenFormUpdate, setStateOpenFormUpdate] = useState(false);
    const [stateOpenCard, setStateOpenCard] = useState(false)
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [status, setStatus] = useState(0)
    const [editTitle, setEditTitle] = useState("")
    const [editDescription, setEditDescription] = useState("")
    const [editStatus, setEditStatus] = useState(0)

    const handleSubmitTODO = (e) => {
        e.preventDefault();
        const data = {
            createdAt: moment().format("YYYY-MM-DD hh:mm"),
            description: description,
            id: (stateList.length + 1),
            status: status,
            title: title,
        }
        setStateList([...stateList, data]);
    }

    const handleSubmitUpdate = (e, id) => {
        e.preventDefault();
        let array = stateList;
        console.log(array);
        let objIndex = array.findIndex((obj => obj.id === id));
        array[objIndex].title = (isEmpty(editTitle)) ? activeCard?.title : editTitle;
        array[objIndex].description = (isEmpty(editDescription)) ? activeCard?.description : editDescription;
        array[objIndex].status = (isEmpty(editStatus)) ? parseInt(activeCard?.status) : parseInt(editStatus);
        setStateList(array);
        handleCloseFormUpdate();
        handleCloseCard();
    }

    const handleDeleteTODO = (id) => {
        const data = stateList.filter(function(item) {
            return item.id !== id
        })
        setStateList(data);

        handleCloseCard();
    }

    const getDataListTODO = async () => {
        const res = await getListTodo('to-do-list');
        // dispatch(addTodo(res))
        setStateList(res)

    }

    const handleOpenFormCreate =  () => {
        setStateOpenFormCreate(true);
    }


    const handleCloseFormUpdate = () => {
        setStateOpenFormUpdate(false);
    }

    const handleOpenFormUpdate = () => {
        setStateOpenFormUpdate(true);
    }

    const handleCloseFormCreate =  () => {
        setStateOpenFormCreate(false);
    }

    const handleOpenCard =  (active) => {
        setActiveCard(active);
        setStateOpenCard(true);
    }

    const handleCloseCard = () => {
        setStateOpenCard(false);
    }

    useEffect(() => {
        getDataListTODO();
    }, [])

    return (
        <>
            <Container>
                <Header title='TODO LIST APP'/>
                <Button onClick={handleOpenFormCreate} className={`mb-2`} variant={`primary`}>Create new</Button>
                <Row>
                    <GroupCard
                        header="Preparation"
                        stateList={stateList}
                        openCard={handleOpenCard}
                        type={0}
                    />
                    <GroupCard
                        header="Doing"
                        stateList={stateList}
                        openCard={handleOpenCard}
                        type={1}
                    />
                </Row>
            </Container>
            <Modal show={stateOpenCard} onHide={handleCloseCard}>
                <Modal.Header closeButton >
                    <Modal.Title>{activeCard?.title}</Modal.Title>
                </Modal.Header>
                <ModalBody>
                    <DetailCard
                        {...activeCard}
                        openFormUpdate={handleOpenFormUpdate}
                        deleteTODO={handleDeleteTODO}

                    />
                </ModalBody>
            </Modal>

            <Modal show={stateOpenFormCreate} onHide={handleCloseFormCreate}>
                <Modal.Header closeButton >
                    <Modal.Title>Create New</Modal.Title>
                </Modal.Header>
                <ModalBody>
                    <form onSubmit={handleSubmitTODO}>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Title</Form.Label>
                            <Form.Control onChange={(e) => setTitle(e.target.value)} type="text"  />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Description</Form.Label>
                            <Form.Control onChange={(e) => setDescription(e.target.value)} as="textarea" rows={3} />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Status</Form.Label>
                            <select onChange={(e) => setStatus(e.target.value)} className="form-control">
                                <option value="0">Preparation</option>
                                <option value="1">Doing</option>
                            </select>
                        </Form.Group>
                        <Form.Group  className="mb-3" style={{textAlign: 'right'}} controlId="exampleForm.ControlTextarea1">
                            <Button variant="primary" type="submit">Save</Button>
                        </Form.Group>
                    </form>
                </ModalBody>
            </Modal>
            <Modal show={stateOpenFormUpdate} onHide={handleCloseFormUpdate}>
                <Modal.Header closeButton >
                    <Modal.Title>Update</Modal.Title>
                </Modal.Header>
                <ModalBody>
                    <form onSubmit={(e) => handleSubmitUpdate(e, activeCard?.id)}>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Title</Form.Label>
                            <Form.Control defaultValue={activeCard?.title} onChange={(e) => setEditTitle(e.target.value)} type="text"  />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Description</Form.Label>
                            <Form.Control defaultValue={activeCard?.description} onChange={(e) => setEditDescription(e.target.value)} as="textarea" rows={3} />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Status</Form.Label>
                            <select onChange={(e) => setEditStatus(e.target.value)} className="form-control">
                                <option selected={!activeCard?.status && false} value="0">Preparation</option>
                                <option selected={activeCard?.status && true} value="1">Doing</option>
                            </select>
                        </Form.Group>
                        <Form.Group  className="mb-3" style={{textAlign: 'right'}} controlId="exampleForm.ControlTextarea1">
                            <Button variant="primary" type="submit">Save</Button>
                        </Form.Group>
                    </form>
                </ModalBody>
            </Modal>
        </>
    )
}

export default Home;

